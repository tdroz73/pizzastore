package pizza

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

import scala.collection.mutable


object PizzaStore {
  def props: Props = Props[PizzaStore]
}

/**
  * Basic PizzaStore supervisor actor.
  *
  * You can Send a PlaceOrder message to place an order and then a SubmitPayment message to submit payment for your order.
  *
  * It uses child actors to handle the gory details of processing various payment types
  *
  * Will send a Bill back to the sender of the PlaceOrder message
  *
  * Will send a Receipt back to the sender of the SubmitPayment message.
  */
class PizzaStore extends Actor with ActorLogging {
  // child actors
  val cashPaymentHandler: ActorRef = context.actorOf(CashPaymentHandler.props)
  val paypalPaymentHandler : ActorRef = context.actorOf(PaypalPaymentHandler.props)
  val venmoPaymentHandler : ActorRef = context.actorOf(VenmoPaymentHandler.props)
  val creditCardPaymentHandler : ActorRef = context.actorOf(CreditCardPaymentHandler.props)

  override def receive: Receive = {
    case PlaceOrder(order) if order.orderItems.nonEmpty =>
      log.debug(s"Received order #${order.orderNumber}")
      val total = order.orderItems.map(_.price).sum
      sender() ! Bill(amountDue = total, order = order)

    case PlaceOrder(order) =>
      log.debug(s"No items for order#${order.orderNumber}")
      sender() ! OrderError(message = "No items on order!", orderNumber = order.orderNumber)

    case SubmitPayment(payment, bill) if payment.amountProvided >= bill.amountDue =>
      log.debug(s"Payment processing for order#${payment.order.orderNumber}")
      val originator = sender()
      val processPaymentMsg = ProcessPayment(payment, bill, originator)
      payment.paymentType match {
        case Cash => cashPaymentHandler ! processPaymentMsg
        case PayPal => paypalPaymentHandler ! processPaymentMsg
        case Venmo => venmoPaymentHandler ! processPaymentMsg
        case CreditCard(_, _, _, _, _, _, _) => creditCardPaymentHandler ! processPaymentMsg
      }

    case SubmitPayment(payment, _) =>
      log.debug(s"Payment amount due doesn't match bill amount due for order#${payment.order.orderNumber}")
      sender() ! PaymentError(message = "Payment amount due doesn't match bill amount due!", orderNumber = payment.order.orderNumber)

    case PaymentProcessingResults(Some(receipt), _, true, message, returnTo) =>
      log.debug(s"Payment processing a success: $message")
      returnTo ! receipt

    case PaymentProcessingResults(None, orderNumber, true, message, returnTo) =>
      log.error(s"Payment processing success - but no receipt?! order#$orderNumber")
      returnTo ! PaymentError(message = "An unknown error occurred with your payment.  Please try again.", orderNumber = orderNumber)

    case PaymentProcessingResults(_, orderNumber, false, message, returnTo) =>
      log.debug(s"Payment processing failed: $message")
      returnTo ! PaymentError(message = message, orderNumber = orderNumber)
    case unexpected => log.error(s"Unexpected message received: $unexpected")
  }
}

// child actors

object CashPaymentHandler {
  def props: Props = Props[CashPaymentHandler]
}

/**
  * Handle cash payments given a ProcessPayment message
  */
class CashPaymentHandler extends Actor with ActorLogging {
  override def receive: Receive = {
    case ProcessPayment(payment, bill, returnTo) =>
      log.debug(s"Processing cash payment for order#${payment.order.orderNumber}")
      val change = payment.amountProvided - bill.amountDue
      val receipt = Receipt(amountPaid = payment.amountProvided, change = change, order = payment.order)
      sender() ! PaymentProcessingResults(receipt = Option(receipt), orderNumber = payment.order.orderNumber, success = true, message = "Cash payment successful!", returnTo = returnTo)
  }
}


object PaypalPaymentHandler {
  def props : Props = Props[PaypalPaymentHandler]
}

/**
  * Handle PayPal payments given a ProcessPayment message
  */
class PaypalPaymentHandler extends Actor with ActorLogging {
  override def receive: Receive = {
    case ProcessPayment(payment, bill, returnTo) =>
      log.debug(s"Processing PayPal payment for order #${payment.order.orderNumber}")
      // this would call out to paypal and setup a payment returning a token, etc
      val receipt = Receipt(amountPaid = bill.amountDue, change = 0.0f, order = payment.order)
      sender() ! PaymentProcessingResults(receipt = Option(receipt), orderNumber = payment.order.orderNumber, success = true, message = "PayPal payment successful.", returnTo = returnTo)
  }
}

object VenmoPaymentHandler {
  def props : Props = Props[VenmoPaymentHandler]
}

/**
  * Handle a Venmo payment given a ProcessPayment message
  */
class VenmoPaymentHandler extends Actor with ActorLogging {
  override def receive: Receive = {
    case ProcessPayment(payment, bill, returnTo) =>
      log.debug(s"Processing Venmo payment for order#${payment.order.orderNumber}")
      // this would do all the necessary calls to venmo to validate payment
      val receipt = Receipt(amountPaid = bill.amountDue, change = 0.0f, order = payment.order)
      sender() ! PaymentProcessingResults(receipt = Option(receipt), orderNumber = payment.order.orderNumber, success = true, message = "Venmo payment successful", returnTo = returnTo)
  }
}

object CreditCardPaymentHandler {
  def props : Props = Props[CreditCardPaymentHandler]
}

/**
  * Handle a Creditcard payment given a ProcessPayment message
  */
class CreditCardPaymentHandler extends Actor with ActorLogging {
  override def receive: Receive = {
    case ProcessPayment(payment, bill, returnTo) =>
      log.debug(s"Processing credit card payment for order#${payment.order.orderNumber}")
      // this would do all the necessary work with a credit card payment gateway
      val receipt = Receipt(amountPaid = bill.amountDue, change = 0.0f, order = payment.order)
      sender() ! PaymentProcessingResults(receipt = Option(receipt), orderNumber = payment.order.orderNumber, success = true, message = "Credit Card payment successful", returnTo = returnTo)
  }
}
