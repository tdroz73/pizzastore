package pizza

import java.util.UUID

sealed trait SpecialtyPizza
case object MeatLovers extends SpecialtyPizza
case object Vegetarian extends SpecialtyPizza
case object Hawaiian extends SpecialtyPizza

sealed trait PizzaSize
case object S extends PizzaSize
case object M extends PizzaSize
case object L extends PizzaSize
case object XL extends PizzaSize

sealed trait OrderDelivery
case object Delivery extends OrderDelivery
case object PickUp extends OrderDelivery
case object DineIn extends OrderDelivery

case class Topping(name : String, halfOnly : Boolean = false)

case class Pizza(toppings : Set[Topping], size : PizzaSize, isSpecialty : Boolean = false)
object Pizza {
  val specialities: Set[SpecialtyPizza] = Set(MeatLovers, Vegetarian, Hawaiian)
  def createSpecialty(specialty: SpecialtyPizza, size: PizzaSize) : Pizza = specialty match {
    case MeatLovers => Pizza(Set(Topping(name = "sausage"), Topping(name = "pepperoni"), Topping(name = "Canadian Bacon")), size = size, isSpecialty = true)
    case Vegetarian => Pizza(Set(Topping(name = "black olives"), Topping(name = "mushrooms"), Topping(name = "onions"), Topping(name = "green peppers")),size = size, isSpecialty = true)
    case Hawaiian => Pizza(Set(Topping(name= "pepperoni"), Topping(name = "pineapple")), size = size, isSpecialty = true)
  }
}

case class OrderItem(pizza: Pizza, price : Float)
case class Order(orderNumber : String = UUID.randomUUID().toString, orderItems : Seq[OrderItem], name : String, deliveryMode : OrderDelivery = DineIn)

// receipt, payment
case class Receipt(amountPaid : Float, change : Float, order : Order)
case class Bill(amountDue : Float, order : Order)

sealed trait PaymentType
case class CreditCard(vendor : String, number : String, expiration : String, cvv : String, zipCode : String, firstName : String, lastName: String) extends PaymentType
case object PayPal extends PaymentType
case object Venmo extends PaymentType
case object Cash extends PaymentType

case class Payment(amountProvided : Float, order : Order, paymentType : PaymentType)
