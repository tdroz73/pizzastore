package pizza

import akka.actor.ActorRef

case class PlaceOrder(order : Order)
case class OrderError(message : String, orderNumber : String)

case class SubmitPayment(payment: Payment, bill: Bill)
case class PaymentError(message : String, orderNumber : String)

case class ProcessPayment(payment: Payment, bill: Bill, returnTo : ActorRef)
case class PaymentProcessingResults(receipt: Option[Receipt], orderNumber : String, success : Boolean, message : String, returnTo: ActorRef)
