package pizza

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfter, Matchers, WordSpecLike}

class PizzaStoreSpec extends TestKit(ActorSystem("PizzaStoreSpecSystem")) with ImplicitSender with WordSpecLike with Matchers with BeforeAndAfter {

  trait BaseFixture {
    val basicPizza = Pizza(toppings = Set(Topping("pepperoni")), size=M)
    val basicPizzaPrice = 10.00f
  }

  trait PositiveOrderFixture extends BaseFixture {
    val order = Order(orderItems = Seq(OrderItem(pizza = basicPizza, price = basicPizzaPrice)), name = "Unit Test")
    val expectedBill = Bill(amountDue = basicPizzaPrice, order = order)
  }

  trait NegativeOrderFixture extends BaseFixture {
    val order = Order(orderItems = Seq.empty[OrderItem], name = "Unit Test")
    val expectedMsg = OrderError(message = "No items on order!", orderNumber = order.orderNumber)
  }

  trait CashPaymentFixutre extends BaseFixture with PositiveOrderFixture {
    val payment = Payment(amountProvided = 20.00f, order = order, paymentType = Cash)
    val expectedReceipt = Receipt(amountPaid = 20.00f, change = 10.00f, order = order)
  }

  "A PizzaStore" must {
    "Accept an order" in new PositiveOrderFixture {
      val pizzaStore = system.actorOf(PizzaStore.props)
      pizzaStore ! PlaceOrder(order)
      expectMsg(expectedBill)
    }
    "Reject an invalid order" in new NegativeOrderFixture {
      val pizzaStore = system.actorOf(PizzaStore.props)
      pizzaStore ! PlaceOrder(order)
      expectMsg(expectedMsg)
    }
    "Process a cash payment" in new CashPaymentFixutre {
      val pizzaStore = system.actorOf(PizzaStore.props)
      pizzaStore ! SubmitPayment(payment, expectedBill)
      expectMsg(expectedReceipt)
    }
  }
}
